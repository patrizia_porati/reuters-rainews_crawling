import urllib2

import datetime
import pymysql.cursors
import re

def specialCharacters (text):
	"""this function replace some HTML quotation marks and special characters"""
	
	#delete html tags
	text = re.sub("<.*?>","",text)
	#replace some html special characters
	text = text.replace("&lsquo;","'")
	text = text.replace("&rsquo;","'")
	text = text.replace("&sbquo",",")
	text = text.replace('&ldquo;', '"')
	text = text.replace('&rdquo;','"')
	text = text.replace("&#x27;", "'")
	text = text.replace("&rdquo;", '"')
	text = text.replace("&quot;", '"')
	text = text.replace("&amp;", "&")
	text = text.replace("&lt","<")
	text = text.replace("&gt",">")
	
	text = text.decode("utf-8")
	
	#delete 'zero-width joiner' characters
	text = text.encode('cp850','xmlcharrefreplace').decode('cp850')
	text = text.replace("&#8205;","")
	text = text.replace("&#8203;","")
	
	text = text.replace("&#8217;","'")
	text = text.replace("&#8221;",'"')
	text = text.replace(" &#8220;",'"')
	
	return text

def fromReuters ():

	"""Save articles from www.reuters.it into the database 'articlesDB':
	table articles (
		Id			VARCHAR(30) NOT NULL,
		Date		DATE,
		Time		TIME,
		Source		VARCHAR(30),
		Title		TEXT,
		Content		LONGTEXT,
		KEY VALUE (Id),
		);"""
	#open connection with the database
	connection = pymysql.connect(host='localhost', user='root', password='mamma93', db='mercurio', charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

	print('Connected with the DB \n')
	print("From www.reuters.it \n")


	#extract articles from the date 'inizio' to today
	inizio = datetime.date(2008,1,1)
	#fine = datetime.date(2017,10,25)
	fine = datetime.date.today()
	oneDay = datetime.timedelta(days=1)

	while inizio <= fine:
		#get the web page
		date = inizio.strftime("%m%d%Y")
		url = "http://it.reuters.com/investing/news/archive/italianNews?date="+ str(date)
		page = urllib2.urlopen(url)

		txt = page.read()

		#find the articles section (b is needed to convert the str in bytes)
		start = txt.find(b'<div class="module">')
		end = txt.find(b'<script language="Javascript">',start)
		articles = txt[start:end]

		startLink = articles.find(b'/article/')
		while startLink != -1:
			#single articles
			endLink = articles.find(b'"  >',startLink)
			link = 'http://it.reuters.com' + articles[startLink:endLink]
			print("Link: " + link)

			"""the last 30 characters of the link, that are the only characters that
			change from the link of the another articles"""
			articleId = articles[startLink+21:endLink]
			print("ID: " + articleId)

			articlePage = urllib2.urlopen(link).read()
			startLink = articles.find(b'/article/',endLink)

			#extract timestamp
			startDate = articlePage.find(b'<div class="ArticleHeader_date_V9eGk" data-reactid="14">')
			endDate = articlePage.find(b' / ', startDate)
			date = inizio
			print("Date: " + date.strftime("%d %B %Y"))

			startTime = endDate + 4
			endTime = articlePage.find(b' / ', startTime)
			#time = articlePage[startTime:endTime]

			hour = int(articlePage[startTime:startTime+2])
			minute = int(articlePage[endTime-2:endTime])
			time = datetime.time(hour,minute)
			print("Time: " + time.strftime("%H:%M"))

			#extract the title
			startTitle = articlePage.find(b'<h1 class="ArticleHeader_headline_2zdFM" data-reactid="15">')
			endTitle = articlePage.find(b'</h1>',startTitle)
			title = articlePage[startTitle+59:endTitle]
			title = specialCharacters(title)
			print("Title: " + title)

			#extract article
			startArticle = articlePage.find(b'<div class="ArticleBody_body_2ECha" data-reactid="31">')
			endArticle = articlePage.find(b'class="TwoColumnLayout_column_1uZPg TwoColumnLayout_right_3kxIR"',startArticle)
			article = articlePage[startArticle:endArticle]
			#print(article)

			#print(article)
			text = ""
			startParagraph = article.find(b'<p data-reactid="')
			while startParagraph != -1:
				endParagraph = article.find(b'</p>',startParagraph)
				paragraph = "\n" + article[startParagraph+21:endParagraph]
				startParagraph = article.find(b'<p data-reactid="',endParagraph)
				text = text + paragraph

			#replace HTML quotation marks
			text = specialCharacters(text)

			#print("Text: " + text)

			#store the article in the db
			try:
				with connection.cursor() as cursor:
					#check that the article isn't already in the db
					query = "SELECT Id FROM articles WHERE Id = %s"
					cursor.execute(query, (articleId))
					result = cursor.fetchone()
					
					#insert the tuple in the db
					if result == None:
						query = "INSERT INTO articles (Id, Date, Time, Source, Title, Content, Link) VALUES (%s, %s, %s, 'www.reuters.it', %s, %s, %s)"
						cursor.execute(query, (articleId,date,time,title,text,link))
						connection.commit()
					else: 
						print("Already in the db!")
			except:
				print('Failed')
			
			print("\n")
			
		inizio = inizio + oneDay

	connection.close()
	print("Connection closed")

fromReuters()
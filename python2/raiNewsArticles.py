import urllib2
import datetime
import pymysql.cursors
import re

def specialCharacters (text):
	"""this function replace some HTML quotation marks and special characters"""
	
	#delete html tags
	text = re.sub("<.*?>","",text)
	#replace some html special characters
	text = text.replace("&lsquo;","'")
	text = text.replace("&rsquo;","'")
	text = text.replace("&sbquo",",")
	text = text.replace('&ldquo;', '"')
	text = text.replace('&rdquo;','"')
	text = text.replace("&#x27;", "'")
	text = text.replace("&rdquo;", '"')
	text = text.replace("&quot;", '"')
	text = text.replace("&amp;", "&")
	text = text.replace("&lt","<")
	text = text.replace("&gt",">")
	
	text = text.decode("utf-8")
	
	#delete 'zero-width joiner' characters
	text = text.encode('cp850','xmlcharrefreplace').decode('cp850')
	text = text.replace("&#8205;","")
	text = text.replace("&#8203;","")
	
	text = text.replace("&#8217;","'")
	text = text.replace("&#8221;",'"')
	text = text.replace(" &#8220;",'"')
	
	return text

def fromRaiNews():
	"""Save articles from www.rainews.it into the database 'articlesDB':
	table articles (
		Id			VARCHAR(40) NOT NULL,
		Date		DATE,
		Time		TIME,
		Source		VARCHAR(40),
		Title		TEXT,
		Content		LONGTEXT,
		Link		TEXT,
		KEY VALUE (Id),
		);"""

	#open connection with the database
	connection = pymysql.connect(host='localhost', user='root', password='mamma93', db='mercurio', charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

	print('Connected with the DB \n')
	print("From www.rainews.it \n")
	
	#first article ---> fine ---> inizio ---> today
	fine = datetime.date(2008,1,1)
	inizio = datetime.date.today()
	#inizio = datetime.date(2017,11,20)
	counter = 0
	while True:
		#get web page
		pageLink = "http://www.rainews.it/dl/rainews/redazioni/ContentSet-37d48870-e750-443f-9139-ff587e10e25c-" + str(counter) + ".html?refresh_ce"
		print(pageLink)
		
		#try to open and read the page
		try:
			page = urllib2.urlopen(pageLink)
			txt = page.read()
		#after the last page, if the page does not exist and then the function returns
		except urllib2.HTTPError, e:
			print("Page finished")
			return

		counter = counter +1
		
		start = txt.find(b'<li class="articolo">')
		while start != -1:
			#find the article section
			end = txt.find(b'</li>',start)
			article = txt[start:end]
			
			startLink = article.find(b'<a class="img" href="')
			endLink = article.find(b'"><img src="',startLink)
			link = "http://www.rainews.it" + article[startLink+21:endLink]
			print("Link: "+ link)
			
			articlePage = urllib2.urlopen(link).read()
			
			#Id (take the last 36 characters from the link)
			id = article[endLink-41:endLink-5]
			print("ID: " + id)
			
			#extract timestamp
			startDate = articlePage.find(b'<time datetime="', articlePage.find(b'<div class="text">'))+16
			endDate = articlePage.find(b'</time>',startDate)
			year = int(articlePage[startDate:startDate+4])
			month = int(articlePage[startDate+5:startDate+7])
			day = int(articlePage[startDate+8:startDate+10])
			
			date = datetime.date(year,month,day)
			print("Date: " + date.strftime("%d %B %Y"))
			# first article ---> fine ---> inizio ---> today
			# if the date of the article is before the date 'fine', then return
			if date < fine:
				print("\n")
				return
			# if the date of the article is after the date 'inizio' then break
			if date > inizio:
				print("\n")
				break
			
			hour = int(articlePage[startDate+11:startDate+13])
			minute = int(articlePage[startDate+14:startDate+16])
			
			time = datetime.time(hour,minute)
			print("Time: " + time.strftime("%H:%M"))
			
			#extract the title
			startTitle = articlePage.find(b'<div class="title">')
			
			startH2 = articlePage.find(b'<h2>',startTitle) + 4
			endH2 = articlePage.find(b'</h2>',startH2)
			preTitle = ""
			if (endH2 - startH2) > 2:
				preTitle = articlePage[startH2:endH2] + "\n"
			
			startH1 = articlePage.find(b'<h1 itemprop="headline">',endH2) + 24
			endH1 = articlePage.find(b'</h1>',startH1)
			headline = articlePage[startH1:endH1]
			
			startH3 = articlePage.find(b'<h3 class="sub">',endH1)
			subtitle = ""
			if startH3 != -1:
				endH3 = articlePage.find(b'</h3>',startH3)
				subtitle = "\n" + articlePage[startH3+16:endH3]

			title = headline 
			title = specialCharacters(title)
			print("Title: " + title)

			#content of the article
			endContent = articlePage.find(b'</div>',endDate)
			content = articlePage[endDate+7:endContent]
			content.replace('"<br>', '\n')
			content.replace("<br>", '\n')
			content.replace('<br>"','\n')
			content = specialCharacters(content)
			
			#store the article in the DB
			try:
				with connection.cursor() as cursor:
					#check that the article isn't already in the db
					query = "SELECT Id FROM articles WHERE Id = %s"
					cursor.execute(query, (id))
					result = cursor.fetchone()
					
					#insert the tuple in the db
					if result == None:
						query = "INSERT INTO articles (Id, Date, Time, Source, Title, Content, Link) VALUES (%s, %s, %s, 'www.rainews.it', %s, %s, %s)"
						cursor.execute(query, (id,date,time,title,content,link))
						connection.commit()
						print("Tuple stored")
					else: 
						print("Already in the db!")
			except:
				print('Failed')
			
			print("\n")
			start = txt.find(b'<li class="articolo">',end)
			
		articlesWithPhoto(txt)
	connection.close()



#if in the page there are articles with a photo, it saves the article in the database
def articlesWithPhoto(txt):

	#open connection with the database
	connection = pymysql.connect(host='localhost', user='root', password='mamma93', db='mercurio', charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
	#print('Connected with the DB \n')

	start = txt.find(b'<li class="foto">')
	while start != -1:
		#find the article section
		end = txt.find(b'</li>',start)
		article = txt[start:end]

		startLink = article.find(b'href="')
		endLink = article.find(b'">',startLink)
		link = "http://www.rainews.it" + article[startLink+6:endLink]
		print("ARTICLE WITH PHOTO  \nLink: "+ link)
		
		articlePage = urllib2.urlopen(link).read()
		
		#Id (take the last 36 characters from the link)
		id = article[endLink-41:endLink-5]
		print("ID: " + id)
		
		#extract timestamp
		startDate = article.find(b'<strong>',endLink)+8
		endDate = article.find(b'</strong>',startDate)
		year = int(article[startDate:startDate+4])
		month = int(article[startDate+5:startDate+7])
		day = int(article[startDate+8:startDate+10])
		
		date = datetime.date(year,month,day)
		print("Date: " + date.strftime("%d %B %Y"))

		#this articles do not have publication time
		
		#extract the title
		startTitle = articlePage.find(b'<h1>', articlePage.find(b'<article>'))
		endTitle = articlePage.find(b'</h1>',startTitle)
		title = articlePage[startTitle+4:endTitle]
		print("Title: " + title)

		#content of the article
		startContent = articlePage.find(b'<p>',endTitle)
		endContent = articlePage.find(b'</p>',startContent)
		content = articlePage[startContent+3:endContent]
		content.replace('"<br>', '\n')
		content.replace("<br>", '\n')
		content.replace('<br>"','\n')
		content = specialCharacters(content)
		#print(content)
		
		#store the article in the DB
		try:
			with connection.cursor() as cursor:
				#check that the article isn't already in the db
				query = "SELECT Id FROM articles WHERE Id = %s"
				cursor.execute(query, (id))
				result = cursor.fetchone()
				
				#insert the tuple in the db
				if result == None:
					query = "INSERT INTO articles (Id, Date, Time, Source, Title, Content, Link) VALUES (%s, %s, NULL, 'www.rainews.it', %s, %s, %s)"
					cursor.execute(query, (id,date,title,content,link))
					connection.commit()
					print("Tuple stored")
				else: 
					print("Already in the db!")
		except:
			print('Failed')
		
		print("\n")
		start = txt.find(b'<li class="foto">',end)
	connection.close()

fromRaiNews()

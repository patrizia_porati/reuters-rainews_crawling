import pymysql.cursors
import openpyxl

#Save the articles in the database
#import reutersArticles
#import raiNewsArticles

"""From the table articles into the database articlesDB, search articles that speak about selected companies (read from ListinoCompleto.xlsx file) and save them into the
table companies (
	articleId	VARCHAR(40) NOT NULL,
	company		TEXT NOT NULL,
	source		VARCHAR(40)
	);"""

#save articles from "www.reuters.it" into the table "articles"
#reutersArticles.fromReuters()

#save articles from "www.rainews.it" into the table "articles"
#raiNewsArticles.fromRaiNews()

#open connection with the database
connection = pymysql.connect(host='localhost', user='root', password='mamma93', db='mercurio', charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
print('Connected with the DB \n')

#read companies from an excel file
excel_document = openpyxl.load_workbook('ListinoCompleto.xlsx')
sheet = excel_document.get_sheet_by_name('Sheet1')

companies = []
row = 1
newCompany = sheet.cell(row = 1, column = 1).value
while newCompany != None:
	companies.append(newCompany)
	row = row+1
	newCompany = sheet.cell(row = row, column = 1).value

print(companies)

try:
	with connection.cursor() as cursor:
		#query on articles table
		query = "SELECT Id, Title, Content, Source FROM articles"
		cursor.execute(query)
		result = cursor.fetchall()

		for record in result:
			for az in companies:
					articleId = record["Id"]
					title = record["Title"].lower()
					content = record["Content"].lower()
					source = record["Source"]

					#look for companies name into the content and into the title of the articles
					markList = [" ", ".", ":", ",", ";", "-", "_", "!", "?", '"', "'", "&", "/", ")", "-"]
					for mark in markList:
						if content.find(mark+az.lower()+mark) != -1 or title.find(mark+az.lower()+mark) != -1:

							#check if the tuble isn't already in the db
							query1 = "SELECT articleId, company, source FROM companies WHERE articleId = %s AND company = %s AND source = %s"
							cursor.execute(query1, (articleId,az,source))
							queryResult = cursor.fetchone();

							#insert the tuple
							if queryResult == None:
								sql = "INSERT INTO companies(articleId, company, source) VALUES (%s, %s, %s)"
								cursor.execute(sql,(articleId,az,source))
								connection.commit()
								print(source + " -> " + articleId + " - " + az)
							
							else:
								print(source + " -> " + articleId + " - " + az + ": ALREADY IN THE DB!")

except:
	print("Failed")

connection.close()